import {Component, OnInit, ViewChild, Input, OnDestroy} from '@angular/core';
import {ConfigurationService, UserService, AppSidebarService, ActivatedUser} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styles: [`
  :host {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-height: 100vh;
  }
  `]
})
export class IndexComponent implements OnInit, OnDestroy {

  constructor(private _userService: UserService,
              private _configurationService: ConfigurationService,
              private _activatedUser: ActivatedUser,
              private _appSidebarService: AppSidebarService) { }

  private userSubscription: Subscription;
  @ViewChild('appSidebarNav') appSidebarNav: any;
  @Input() user: any;
  public today = new Date();
  public languages: any;
  public currentLang: string;
  public applicationImage: string;
  public applicationTitle: string;

  async ngOnInit() {
    this.userSubscription = this._activatedUser.user.subscribe((user: any) => {
     if (user != null) {

         // get sidebar navigation items
         this.appSidebarNav.navItems = (<any>this._appSidebarService).navigationItems;
         // get current user
         this.user = user;
         // get current language
         this.currentLang = this._configurationService.currentLocale;
         // get languages
         this.languages = this._configurationService.settings.i18n.locales;
         // get path of brand logo
         const appSettings: { image?: string; title?: string } = this._configurationService.settings.app;
         this.applicationImage = appSettings && appSettings.image;
         // get application title
         this.applicationTitle = (appSettings && appSettings.title) || 'Universis';

     }
    });
  }

  changeLanguage(lang: any) {
    this._configurationService.currentLocale = lang;
    // reload current route
    window.location.reload();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
