export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Sidebar.Dashboard',
    key: 'Sidebar.Dashboard',
    url: '/home',
    icon: 'fa fa-home',
    index: 0
  },
  {
    name: 'Sidebar.Users',
    key: 'Sidebar.Users',
    url: '/users',
    icon: 'fas fa-user',
    index: 0
  },
  {
    name: 'Sidebar.Groups',
    key: 'Sidebar.Groups',
    url: '/groups',
    icon: 'fas fa-user-friends',
    index: 0
  }
];
