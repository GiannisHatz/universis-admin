import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './layouts/index.component';
import { AuthGuard } from '@universis/common';
import { AdminModule } from '@universis/ngx-admin';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    canActivateChild: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        loadChildren: () => AdminModule
      },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
